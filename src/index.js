import  Nav   from './Print';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

class NewNav extends Nav {
    constructor() {
        super();

    }
    create() {
        this.createLI();
        let mainContent = document.createElement('div');
        mainContent.setAttribute('class', 'main-content');
        document.body.appendChild(mainContent);

    }
}

const navList = new NewNav();
navList.create();

