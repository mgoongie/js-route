import {NavElements} from '../NavElements';
export default class Nav extends NavElements {
    constructor() {
        super()
    }

    createLI() {
        let item = document.createElement('nav');
        item.classList.add('navbar', 'navbar-expand-lg',  'navbar-light', 'bg-light');
        console.log(item);
        let ul = document.createElement('ul');
        item.appendChild(ul);
        ul.classList.add('navbar-nav');
        for(let child of this.router()) {
            ul.appendChild(child);
        }
        document.body.appendChild(item);
    };
}
