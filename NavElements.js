export class  NavElements {
    constructor() {
        this.items = [
            {
                name: 'Home',
                path: '/'
            },
            {
                name: 'About',
                path: '/about'
            },
            {
                name: 'Services',
                path: '/services'
            },
            {
                name: 'Contact',
                path: '/contact'
            }
        ];

    }

    router() {
        let child = [];
        let _this = this;
        for (let values of this.items) {
            let createList = document.createElement('li');
            let anchors = document.createElement('a');
            createList.classList.add('nav-item');
            anchors.setAttribute('route', values.path);
            anchors.setAttribute('href', values.path);
            anchors.innerHTML += values.name;
            anchors.addEventListener('click',  function (e) {
                e.preventDefault();
                window.history.pushState( {}, '', e.target.getAttribute('route'));
                _this.addPath(values.path)
            });
            createList.appendChild(anchors);
            child.push(createList);
        }
        return child
    };
    addPath() {
        localStorage.setItem('home', 'You are at home page');
        localStorage.setItem('about', 'about page');
        localStorage.setItem('services', 'service\'s page');
        localStorage.setItem('contacts', 'contacts');
        let elem = document.getElementsByClassName('main-content')[0];
        console.log('elem: ', elem);

        elem.innerHTML = '';
        let content = null;
        let currentPath = window.location.pathname;
        console.log('currentPath: ', currentPath);
        if (currentPath === '/') {
            content = localStorage.getItem('home');
        }
        else if (currentPath === '/about') {
            content = localStorage.getItem('about');
        }
        else if (currentPath === '/services') {
            content = localStorage.getItem('services');
        }
        else if(currentPath === '/contact') {
            content = localStorage.getItem('contacts');
        }
        elem.append(content);

    }
}
